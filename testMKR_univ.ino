
#include <MKRWAN.h>
#include <CayenneLPP.h>

#define DEBUG 
//#define DHT22
//#define TSL2561C
#define BME680
//#define AIRQUALITY


#include "arduino_secrets.h"
#include "libs.h"

String appEui;
String appKey;
boolean connected;
int datarateEval = 0;
int err_count;

LoRaModem modem;
CayenneLPP lpp(51);

void setup() {
  DEBUG_WAIT();
  pinMode(LED_BUILTIN, OUTPUT);
  flashLed();

  #ifdef TSL2561C
    //Initalisation capteur TSL2561
    Wire.begin();
    TSL2561.init();
    DEBUG_PRINTLN("Capteur TSL2561 configuré");
  #endif

  #ifdef DHT22
    // Initialisation capteur DHT22
    dht.begin();
    DEBUG_PRINTLN("Capteur DHT22 configuré");
  #endif
  
  #ifdef BME680
    //Initialisation BME680
    while (!bme680.init()) 
    {
      DEBUG_PRINTLN("bme680 introuvable");
    delay(10000);
    }
    DEBUG_PRINTLN("Capteur BME680 configuré");
  #endif

  #ifdef AIRQUALITY
    //Initialisation AirQuality
    if (sensor.init()) {
       DEBUG_PRINTLN("DEBUG_PRINTLN configuré");
    }
    else {
      DEBUG_PRINTLN("Erreur DEBUG_PRINTLN");
    }
  #endif

  connected = false;
  err_count=0;
  if (!modem.begin(EU868)) {
    DEBUG_PRINTLN("Impossible de démarrer le module LORA");
    while (1) {}
  };
  delay(1000); //important
  
  DEBUG_PRINT("Le module LORA est en version : ");
  DEBUG_PRINTLN(modem.version());
  DEBUG_PRINT("Votre EUI est : ");
  DEBUG_PRINTLN(modem.deviceEUI());
  setDataRate(0);
  setLoraKeys(modem.deviceEUI());
}


void loop() {
  lpp.reset();
  
  #ifdef DHT22 
    getWeather(); 
  #endif
  
  #ifdef TSL2561C
    getLightIntensity();
  #endif
  
  #ifdef BME680
    getBME680();
  #endif
  
  #ifdef AIRQUALITY
    getAirQuality();
  #endif
  
  if (!connected) {
      modem.minPollInterval(60);
      modem.setADR(true);
      int ret = modem.joinOTAA(appEui, appKey);
      if (ret) {
        connected = true;
        err_count=0;
        delay(100);
        DEBUG_PRINTLN("Connecté au réseau");
        //dynamicDataRate(true);
      } else {
        DEBUG_PRINTLN("Erreur connexion");
        //dynamicDataRate(false);
      }
  DEBUG_PRINTLN(" "); 
  }

  if (connected) {
      int err;
      modem.beginPacket();
      modem.write(lpp.getBuffer(), lpp.getSize());
      err = modem.endPacket(false);
      // Vérification
      if (err > 0) {
        DEBUG_PRINTLN("Message bien envoyé");    
        err_count=0;     
        delay(1000);
        if (!modem.available()) {
          DEBUG_PRINTLN("Pas de message reçu pour le moment");
        } else DEBUG_PRINTLN("Il y a un message reçu");
      } else {
        err_count++;
        DEBUG_PRINT("Erreur n°");
        DEBUG_PRINT(err_count);
        DEBUG_PRINTLN(" lors de l'envoi du message :-(");      
        if ( err_count >= 10 ) {
          connected = false;
          DEBUG_PRINTLN("Demande de reconnexion");
        }
      }
      flashLed();
      DEBUG_PRINTLN("Attente 120s");
      delay(120000);
      DEBUG_PRINTLN(" "); 
  }

}
