#ifdef DEBUG
  #define DEBUG_PRINTLN(x) Serial.println(x)
  #define DEBUG_PRINTLN2(x,y) Serial.println(x,y)
  #define DEBUG_PRINT(x) Serial.print(x)
  #define DEBUG_PRINT2(x,y) Serial.print(x,y)
  #define DEBUG_WAIT() while (!Serial); Serial.begin(115200)

#else
  #define DEBUG_PRINTLN(x)
  #define DEBUG_PRINTLN2(x,y)
  #define DEBUG_PRINT(x)
  #define DEBUG_PRINT2(x,y)
  #define DEBUG_WAIT()
#endif

#ifdef TSL2561C
  //Capteur TSL2561 sur port TWI/I2C
  //https://github.com/Seeed-Studio/Grove_Digital_Light_Sensor/archive/master.zip
  #include <Wire.h>
  #include <Digital_Light_TSL2561.h>
#endif


#ifdef DHT22
  //Capteur DHT22 sur port D2
  //https://github.com/Seeed-Studio/Grove_Temperature_And_Humidity_Sensor
  #include "DHT.h"
  #define DHTPIN 2 
  #define DHTTYPE DHT22
  DHT dht(DHTPIN, DHTTYPE);
#endif

#ifdef BME680
  //Capteur BME680 sur port TWi/I2C
  //https://github.com/Seeed-Studio/Seeed_BME680
  #include <seeed_bme680.h>
  #define IIC_ADDR  uint8_t(0x76)
  Seeed_BME680 bme680(IIC_ADDR);
#endif

#ifdef AIRQUALITY
  //Capteur Air Quality Sensor v1.3 sur port A0
  //https://github.com/Seeed-Studio/Grove_Air_quality_Sensor
  #include "Air_Quality_Sensor.h"
  AirQualitySensor sensor(A0);
#endif
