void setLoraKeys(String thisDEV_EUI) {
  for (int i=0; i<sizeof(key)/sizeof(struct Keys); i++) {
    if (key[i].DEV_EUI == thisDEV_EUI) {
      appEui = key[i].APP_EUI;
      DEBUG_PRINT("APP_EUI: ");
      DEBUG_PRINTLN(appEui);
      appKey = key[i].APP_KEY;
      DEBUG_PRINT("APP_KEY: ");
      DEBUG_PRINTLN(appKey);
      return;
      }
    }
   DEBUG_PRINTLN("Pas de correspondance DEV_EUI trouvé ");
   while(1);
}

void setDataRate(int DataR) {
  bool datarate = modem.dataRate(DataR); 
  if (!datarate) {
    DEBUG_PRINTLN("DataRate not set");
  }
  
  DEBUG_PRINT("Le datarate est: ");
  DEBUG_PRINTLN(modem.getDataRate());
  DEBUG_PRINTLN(" ");
}

void dynamicDataRate(boolean result){
  if (result == true){
    datarateEval -= 1;      //Plus dataEval est petit, moins il y a d'erreurs
  } else datarateEval += 1; //Plus dataEval est grand, plus il y a d'erreurs
  
  
  DEBUG_PRINT("Niveau DataRate: ");
  DEBUG_PRINTLN(datarateEval);
  if (datarateEval == 10) {
    DEBUG_PRINTLN("Beaucoup d'erreur -> Datarate 0");
    setDataRate(0);
    datarateEval= 0;
    DEBUG_PRINTLN(" ");
  }
  if (datarateEval == -10) {
    DEBUG_PRINTLN("Pas beaucoup d'erreurs -> Datarate 5");
    setDataRate(5);
    datarateEval= 0;
    DEBUG_PRINTLN(" ");
  }
        
}

void flashLed(){
  digitalWrite(LED_BUILTIN, HIGH);   
  delay(1000);                       
  digitalWrite(LED_BUILTIN, LOW);    
  delay(1000);                       
}

#ifdef BME680
  void getBME680(){
     if (bme680.read_sensor_data()){
      DEBUG_PRINTLN("Impossible de lire BME680");
      return;
    }
    DEBUG_PRINT("Humiditée: "); 
    DEBUG_PRINT(bme680.sensor_result_value.humidity);
    DEBUG_PRINTLN("%\t");
    lpp.addRelativeHumidity(1, (float)bme680.sensor_result_value.humidity); //Humidité Channel 1
    
    DEBUG_PRINT("Temperature: "); 
    DEBUG_PRINT(bme680.sensor_result_value.temperature);
    DEBUG_PRINTLN("°C");
    lpp.addTemperature(2, (float)bme680.sensor_result_value.temperature);      //Température Channel 2
  
    DEBUG_PRINT("Pression: "); 
    DEBUG_PRINT(bme680.sensor_result_value.pressure/ 1000.0);
    DEBUG_PRINTLN("KPa");
    lpp.addBarometricPressure(4, (float)bme680.sensor_result_value.pressure/100); //pression en HPA channel 4
  
    DEBUG_PRINT("GAS: "); 
    DEBUG_PRINT(bme680.sensor_result_value.gas/ 1000.0);
    DEBUG_PRINTLN("Kohms");
    //lpp.addLuminosity(3, (uint16_t)total / numReadings); //Luminosité channel 3
    DEBUG_PRINTLN(" ");
  }
#endif

#ifdef AIRQUALITY
  void getAirQuality(){
    int quality = sensor.slope();
  
    DEBUG_PRINT("Air quality sensor value: ");
    DEBUG_PRINTLN(sensor.getValue());
    
    if (quality == AirQualitySensor::FORCE_SIGNAL) {
      DEBUG_PRINTLN("High pollution! Force signal active.");
    }
    else if (quality == AirQualitySensor::HIGH_POLLUTION) {
      DEBUG_PRINTLN("High pollution!");
    }
    else if (quality == AirQualitySensor::LOW_POLLUTION) {
      DEBUG_PRINTLN("Low pollution!");
    }
    else if (quality == AirQualitySensor::FRESH_AIR) {
      DEBUG_PRINTLN("Fresh air.");
    }
  }
#endif

#ifdef DHT22
  void getWeather(){
    float h = dht.readHumidity();
    float t = dht.readTemperature();
  
    if (isnan(t) || isnan(h)) 
      {
          DEBUG_PRINTLN("Impossible de lire le DHT");
      } 
    else 
      {
          DEBUG_PRINT("Humiditée: "); 
          DEBUG_PRINT(h);
          DEBUG_PRINTLN("%\t");
          DEBUG_PRINT("Température: "); 
          DEBUG_PRINT(t);
          DEBUG_PRINTLN("°C");
          lpp.addRelativeHumidity(1, (float)h); //Humidité Channel 1
          lpp.addTemperature(2, (float)t);      //Température Channel 2
      }
    DEBUG_PRINTLN(" ");
  }
#endif

#ifdef TSL2561C
  void getLightIntensity(){
      const int numReadings = 10;
      unsigned long readings[numReadings]; 
      int total = 0;
      for (int thisReading = 0; thisReading < numReadings; thisReading++) {
        readings[thisReading] = TSL2561.readVisibleLux();
        total = total + readings[thisReading];
      }
      DEBUG_PRINT("Luminosité: "); 
      DEBUG_PRINT((uint16_t)total / numReadings);
      DEBUG_PRINTLN("LUX");
      lpp.addLuminosity(3, (uint16_t)total / numReadings); //Luminosité channel 3
      DEBUG_PRINTLN(" ");
  }
#endif
