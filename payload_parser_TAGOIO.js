// Add ignorable variables in this array.
const ignore_vars = ['applicationID','applicationName','deviceName','devEUI','rxInfo','txInfo','adr','fCnt','fPort','object'];

/**
 * Convert an object to TagoIO object format.
 * Can be used in two ways:
 * toTagoFormat({ myvariable: myvalue , anothervariable: anothervalue... })
 * toTagoFormat({ myvariable: { value: myvalue, unit: 'C', metadata: { color: 'green' }} , anothervariable: anothervalue... })
 *
 * @param {Object} object_item Object containing key and value.
 * @param {String} serie Serie for the variables
 * @param {String} prefix Add a prefix to the variables name
 */

function toTagoFormat(object_item, serie, prefix = '') {
  const result = [];
  for (const key in object_item) {
    if (ignore_vars.includes(key)) continue; // ignore chosen vars

    if (typeof object_item[key] == 'object') {
      result.push({
        variable: object_item[key].variable || `${prefix}${key}`,
        value: object_item[key].value,
        serie: object_item[key].serie || serie,
        metadata: object_item[key].metadata,
        unit: object_item[key].unit,
        location: object_item[key].location,
      });
    } else {
      result.push({
        variable: `${prefix}${key}`,
        value: object_item[key],
        serie,
      });
    }
  }

  return result;
}


function parsePayload(payload_raw) {
  const buffer = Buffer.from(payload_raw, 'base64');
  //cayenne LPP perso
  const data = {
    temperature: { value: buffer.slice(5,7).readInt16BE() / 10, unit: '°C' },
    humidity: { value: buffer.slice(2,3).readUInt8(), unit: '%' },
  };

  return data;
}

//converti le json raw en json "Tago"
if (!payload[0].variable) {
  const serie = payload[0].serie || new Date().getTime();

  payload = toTagoFormat(payload[0], serie);
}


//recupere la variable data pour parser le format cayenne LPP
const my_payload = payload.find(x => x.variable === 'data');
if (my_payload) {
  const { serie, value } = my_payload;
  try {
    const parsed_payload = parsePayload(value);
    payload = payload.concat(toTagoFormat(parsed_payload, serie));
  } catch (e) {
    payload = {  variable: 'parse_error', value: e.message || e };
  }
}


